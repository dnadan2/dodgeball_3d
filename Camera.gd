extends Camera

var initialTranslation
var initialRotation

var player

func _ready():
	initialTranslation = translation
	initialRotation = rotation_degrees
	
	player= $Player

func _process(delta):
	if player != null:
		translation.z =initialTranslation.z + player.translation.z 
		translation.x =initialTranslation.z + player.translation.x
