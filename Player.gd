extends KinematicBody
var targetPos
var racastingHelper = load("res://Raycasting.gd").new()

const MAX_SPEED = 1500
const SPEED_MULTIPLIER = 1500
const MOVEMENT_THRESHOLD = 1
const GRAVITY = 800

func _physics_process(delta):    
	var velocity = Vector3(0,0,0)
	
	if Input.is_mouse_button_pressed(1):
		targetPos = racastingHelper.get_click_pos_on_screen_at_y_0(get_parent().get_node("Camera"), get_viewport().get_mouse_position())
	
	velocity.y -= GRAVITY
	
	if targetPos != null:
		if abs(targetPos.x - translation.x) > MOVEMENT_THRESHOLD:
			velocity.x = velocity_to_target(targetPos.x, translation.x)
		
		if abs(targetPos.z - translation.z) > MOVEMENT_THRESHOLD:
			velocity.z = velocity_to_target(targetPos.z, translation.z)
	
	velocity = move_and_slide(velocity * delta, Vector3.UP)

func velocity_to_target(targetPos, translation):
	var multiplier = 1
		
	if targetPos < translation:
		multiplier = -1
	
	var desiredVelocity = min(MAX_SPEED,  abs((targetPos - translation)) * SPEED_MULTIPLIER)
	
	return multiplier * desiredVelocity
